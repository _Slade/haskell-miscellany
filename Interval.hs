module Interval (
    Interval,
    IntervalType(..),
    contains,
    enumerateRange,
    leftEnd,
    rightEnd,
    endPoints,
    within,
    newInterval,
) where

data IntervalType = Open | Closed | LeftOpen | LeftClosed
    deriving (Show, Eq)

data Interval a
    = OpenInterval       a a
    | ClosedInterval     a a
    | LeftOpenInterval   a a
    | LeftClosedInterval a a
    deriving (Eq, Ord)

instance Show a => Show (Interval a) where
    show i = l ++ show (leftEnd i) ++ ", " ++ show (rightEnd i) ++ r
        where delim (OpenInterval       _ _) = ("(", ")")
              delim (LeftOpenInterval   _ _) = ("(", "]")
              delim (ClosedInterval     _ _) = ("[", "]")
              delim (LeftClosedInterval _ _) = ("[", ")")
              (l, r) = delim i

newInterval :: (Enum a, Ord a) => IntervalType -> a -> a -> Interval a
newInterval t a b =
    let l = min a b
        r = max a b
    in case t of
        Open       -> OpenInterval l r
        Closed     -> ClosedInterval l r
        LeftOpen   -> LeftOpenInterval l r
        LeftClosed -> LeftClosedInterval l r


contains :: Ord a => Interval a -> a -> Bool
contains interval x = case  interval  of
    OpenInterval       a b  ->  a <  x && x <  b
    ClosedInterval     a b  ->  a <= x && x <= b
    LeftOpenInterval   a b  ->  a <  x && x <= b
    LeftClosedInterval a b  ->  a <= x && x <  b

within :: Ord a => a -> Interval a -> Bool
within = flip contains

endPoints :: Interval a -> (a, a)
endPoints interval = case  interval  of
    OpenInterval       a b -> (a, b)
    ClosedInterval     a b -> (a, b)
    LeftOpenInterval   a b -> (a, b)
    LeftClosedInterval a b -> (a, b)

leftEnd :: Interval a -> a
leftEnd interval = a
    where (a, _) = endPoints interval

rightEnd :: Interval a -> a
rightEnd interval = b
    where (_, b) = endPoints interval

-- range :: Num a => Interval a -> a
-- range i = (rightEnd i) - (leftEnd i)

enumerateRange :: Enum a => Interval a -> [a]
enumerateRange i = case  i  of
    OpenInterval       a b -> [succ a .. pred b]
    ClosedInterval     a b -> [     a ..      b]
    LeftOpenInterval   a b -> [succ a ..      b]
    LeftClosedInterval a b -> [     a .. pred b]
