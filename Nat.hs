module Nat (
    Nat(..),
) where

{-# LANGUAGE InstanceSigs #-}

data Nat = Zero | Succ Nat
    deriving (Eq, Ord)

fromNat :: Num a => Nat -> a
fromNat Zero     = 0
fromNat (Succ n) = 1 + fromNat n

fromNum :: (Eq a, Num a) => a -> Nat
fromNum 0 = Zero
fromNum n = Succ . fromNum $ n - 1

instance Show Nat where
    show = show . fromNat

instance Enum Nat where
    toEnum   = fromNum

    fromEnum = fromNat

    succ = Succ

    pred Zero = Zero
    pred (Succ x) = x

instance Num Nat where
    fromInteger = fromNum

    (+) :: Nat -> Nat -> Nat
    Zero + x   = x
    Succ x + y = Succ (x + y)

    (*) :: Nat -> Nat -> Nat
    Zero   * _    = Zero
    Succ _ * Zero = Zero
    Succ x * y    = y + (x * y)

    abs = id

    signum Zero = Zero
    signum _    = Succ Zero

    x - y  = undefined
    negate = undefined
