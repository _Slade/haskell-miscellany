Note: Despite the .lhs extension, this file is not meant to be compilable. LHS
allows for selective syntax highlighting with Haskell notation in most editors,
which is why it's used here.
{{{ Order of definitions
These two definitions mean different things:
Correct
> abs x | x >= 0 =  x
> abs x          = -x
Incorrect
> abs2 x          = -x
> abs2 x | x >= 0 =  x
}}}
{{{ Point-free Style
> sum     = foldr (+) 0
> f       = (+ 1)
versus
> sum' xs = foldr (+) 0 xs
> f' x    = x + 1
}}}
{{{ Uncategorized snippets
> isSorted :: (Ord a) => [a] -> Bool
> isSorted xs = zipWith (<=) xs (tail xs)
}}}
{{{ Lambda expressions
Lambda expressions have the following identity:
λp₁ … pₙ → e ≡ λx₁ … xₙ → case (x₁,…,xₙ) of (p₁, …, pₙ) → e
{#- LANGUAGE LambdaCase -#} allows for code like
> \case
>     foo -> ...
>     bar -> ...
>     _   -> ...
which is shorthand for
> \x -> case x of ...
}}}
{{{ Pattern matching syntax
> case e of { p_1 match_1; ...; p_n match_n }
where each match_i is of the general form
> | gs_i1 -> e_i1
> ...
> | gs_im_i -> e_im_i
> where decls_i

> pat -> exp where decls
is shorthand for
> pat | True -> exp
> where decls

Patterns must be linear; C<< f (x, x) = x >> is not legal, for example.

var@pat denotes an as-pattern; var can be used as a name for the value being
matched by pat.

~ denotes an irrefutable pattern. Some properties of irrefutable patterns:
> (\ ~(x, y) -> 0) ⊥  = 0
> (\  (x, y) -> 0) ⊥  = ⊥
> (\ ~[x]    -> 0) [] = 0
> (\  [x]    -> x) [] = ⊥

Irrefutable patterns are not matched until one of the variables is used.
In other words, they're matched "on demand" only.

There does not appear to be a clean way to express alternation. For example,
> isVowel :: Char -> Bool
> isVowel 'a' = True
> isVowel 'e' = True
> isVowel 'i' = True
> isVowel 'o' = True
> isVowel 'u' = True
> isVowel _   = False
A where clause can be used to deduplicate longer function bodies.
}}}
{{{ Standard typeclasses
{{{ Functor
> class Functor (f :: * -> *) where
>   fmap :: (a -> b) -> f a -> f b
>   (<$) :: a -> f b -> f a
Minimal complete definition: fmap
Laws:
    Identity:
>       fmap id      == id
    Composition:
>       fmap (f . g) == fmap f . fmap g
The first law implies the second.
}}}
{{{ Applicative
> class Functor f => Applicative (f :: * -> *) where
>   pure  :: a -> f a
>   (<*>) :: f (a -> b) -> f a -> f b
>   (*>)  :: f a -> f b -> f b
>   (<*)  :: f a -> f b -> f a
Minimal complete definition: pure, (<*>)
Instances should satisfy the following laws:
    Identity:
>       pure id <*> v     == v
    Homomorphism:
>       pure f <*> pure x == pure (f x)
    Interchange:
>       u <*> pure y      == pure ($ y) <*> u
    Composition:
>       pure (.) <*> u <*> v <*> w == u <*> (v <*> w)
}}}
{{{ Monad
> class Applicative m => Monad (m :: * -> *) where
>   (>>=)  :: m a -> (a -> m b) -> m b
>   (>>)   :: m a -> m b -> m b
>   return :: a -> m a
>   fail   :: String -> m a -- Ignore this one
Minimal complete definition: (>>=)
Instances should satisfy the following laws:
    Left identity:
>       return a >>= k         == k a
    Right identity:
>       m >>= return           == m
    Associativity:
>       m >>= (x -> k x >>= h) == (m >>= k) >>= h
Some identities:
>   x >>= f = join (fmap f x)
>   join . fmap return = id -- Is this one true?
>   join . return = id
>   join x = x >>= id
>   x >>= f = join $ f <$> x
>   fmap f xs = xs >>= return . f
>   (>>) = (*>)
>   pure = return
>   (<*>) = ap
}}}
{{{ Num
> class Num a where
>     {-# MINIMAL (+), (*), abs, signum, fromInteger, (negate | (-)) #-}
>     (+), (-), (*) :: a -> a -> a
>     negate        :: a -> a
>     abs           :: a -> a
>     signum        :: a -> a
>     fromInteger   :: Integer -> a
>     x - y         = x + negate y
>     negate x      = 0 - x
The functions 'abs' and 'signum' should satisfy the law:
>        abs x * signum x == x
}}}
{{{ Monoid
> class Monoid a where
>     mempty  :: a
>     -- ^ Identity of 'mappend'
>     mappend :: a -> a -> a
>     -- ^ An associative operation
>     mconcat :: [a] -> a
>     -- ^ Fold a list using the monoid.
>     mconcat = foldr mappend mempty
The class of monoids (types with an associative binary operation that has an
identity). Instances should satisfy the following laws:
    Left identity:
>       mempty `mappend` x == x
    Right identity:
>       x `mappend` mempty == x
    Associativity:
>       x `mappend` (y `mappend` z) == (x `mappend` y) `mappend` z
    Foldability?:
>       mconcat == foldr mappend mempty

Some types can be viewed as a monoid in more than one way, e.g. both addition
and multiplication on numbers.
In such cases we often define newtypes and make those instances of 'Monoid',
e.g. 'Sum' and 'Product'.
}}}
{{{ Either
> data Either a b = Left a | Right b
> instance Functor (Either a) where
>     fmap _ (Left  x) = Left x
>     fmap f (Right y) = Right (f y)
>
> instance Applicative (Either e) where
>     pure          = Right
>     Left  e <*> _ = Left e
>     Right f <*> r = fmap f r
>
> instance Monad (Either e) where
>     return = Right
>     Left  l >>= _ = Left l
>     Right r >>= k = k r
> either :: (a -> c) -> (b -> c) -> Either a b -> c
> either f _ (Left  x) = f x
> either _ g (Right y) = g y
}}}
{{{ Maybe
> data Maybe a = Nothing | Just a
> instance Monoid a => Monoid (Maybe a) where
>     mempty = Nothing
>     Nothing `mappend` m = m
>     m `mappend` Nothing = m
>     Just m1 `mappend` Just m2 = Just (m1 `mappend` m2)
> instance  Functor Maybe  where
>     fmap _ Nothing  = Nothing
>     fmap f (Just a) = Just (f a)
> instance Applicative Maybe where
>     pure = Just
>     Just f  <*> m   = fmap f m
>     Nothing <*> _m  = Nothing
>     Just _m1 *> m2  = m2
>     Nothing  *> _m2 = Nothing
> instance  Monad Maybe  where
>     (Just x) >>= k  = k x
>     Nothing  >>= _  = Nothing
>     (>>)            = (*>)
>     return          = Just
>     fail _          = Nothing
}}}
}}}
{{{ Exponentiation Operators
> (^) :: (Num a, Integral b) => a -> b -> a -- Defined in ‘GHC.Real’
> (^^) :: (Fractional a, Integral b) => a -> b -> a
> class Fractional a => Floating a where
>  (**) :: a -> a -> a
}}}
{{{ Implement Monad, get Applicative and Functor for free:
> instance Monad _ where ...
> instance Applicative _ where
>     pure  = return
>     (<*>) = ap
> instance Functor _ where
>     fmap f x = x >>= (return . f)
}}}
{{{ Operators
Operators without fixity declarations default to infixl 9.
}}}
{{{ Application operators
> ($)   ::   (a ->   b) -> (  a ->   b)
> (<$>) ::   (a ->   b) -> (f a -> f b)
> (=<<) ::   (a -> m b) -> (m a -> m b)
> (<*>) :: f (a ->   b) -> (f a -> f b)
> ap    :: m (a ->   b) -> (m a -> m b)
}}}
{{{ Type-related operators
Translation to HK:
> e :: t  =  let { v :: t; v = e } in v
CONSTRAINT/CONTEXT => TYPE

(a ~ b) => a -- Condition that is "met" when a and b are the same type
> undefined :: String ~ f a => a -- Char

exp -> infixexp :: [context =>] type
    |  infixexp

:: parses
> f x y :: Int  =  (f x y) :: Int
> \x -> a+b :: Int  =  \x -> ((a+b) :: Int)
}}}
http://dev.stephendiehl.com/hask/

vim:fdm=marker:fmr={{{,}}}:syntax=lhaskell
