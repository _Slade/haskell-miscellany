module Problems where

-- https://wiki.haskell.org/99_questions/1_to_10

-- Problem 1
myLast :: [a] -> a
myLast xs = xs !! pred (length xs)

-- Problem 2
myButLast :: [a] -> a
myButLast (x:_:[]) = x
myButLast (x:xs) = myButLast xs

-- Problem 3
elementAt :: [a] -> Int -> a
elementAt (x:_) 1 = x
elementAt (_:xs) n = elementAt xs $ pred n

-- Problem 4
myLength :: (Num b) => [a] -> b
myLength xs = foldr (\ x y -> 1 + y) 0 xs

myLength' :: (Num b) => [a] -> b
myLength' xs = sum [ 1 | _ <- xs ] -- LYAH

-- Problem 5
myReverse :: [a] -> [a]
myReverse = foldl (\ b c -> c : b) []

-- Problem 6
isPalindrome :: (Eq a) => [a] -> Bool
isPalindrome xs = xs == reverse xs

isPalindrome' :: (Eq a) => [a] -> Bool
isPalindrome' xs = go xs $ reverse xs where {
    go (f:xf) (b:xb) = if f == b then go xf xb else False;
    go (_:[]) _      = True;
    go [] _          = True;
}

isPalindrome'' :: (Eq a) => [a] -> Bool
isPalindrome'' xs = and $ zipWith (==) (take n xs) (drop (n - 1) xs)
    where n = length xs `mod` 2

-- Problem 7
data NestedList a = Elem a | List [NestedList a] deriving (Show)
flatten :: NestedList a -> [a]
flatten (Elem a)      = [a]
flatten (List [])     = []
flatten (List (a:xs)) = flatten a ++ flatten (List xs)

-- Problem 8
compress :: (Eq a, Show a) => [a] -> [a]
compress (a:b:xs)
    | a == b    = compress (a:xs)
    | otherwise = a : compress (b:xs)
compress x = x

-- Problem 9
pack :: (Eq a) => [a] -> [[a]]
pack []       = []
pack xs@(x:_) = pack' $ span (x ==) xs
    where pack' (dups, rest) = dups : pack rest
