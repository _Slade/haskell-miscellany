module Degrees (
    Temperature,
    kelvin,
    fahrenheit,
    celsius,
    toKelvin,
    toFahrenheit,
    toCelsius
) where

data Temperature a =
      Kelvin     a
    | Celsius    a
    | Fahrenheit a
    deriving (Show, Eq)

kelvin, fahrenheit, celsius :: (Num a, Ord a) => a -> Temperature a
kelvin     = smartCtor Kelvin         0
fahrenheit = smartCtor Fahrenheit (-459.67)
celsius    = smartCtor Celsius    (-273.15)

-- smartCtor :: (a -> Temperature a) -> a -> (a -> Temperature a)
smartCtor con az t
    | t < 0 = error "Temperature below absolute zero"
    | otherwise = con t

toKelvin, toCelsius, toFahrenheit :: Fractional t =>
    Temperature t -> Temperature t

toKelvin     (Celsius    t) = Kelvin     $ t + 273.15
toKelvin     (Fahrenheit t) = Kelvin     $ 5 / 9 * (t + 459.67)
toKelvin                 t  = t
toCelsius    (Kelvin     t) = Celsius    $ t - 273.15
toCelsius    (Fahrenheit t) = Celsius    $ 5 / 9 * (t - 32)
toCelsius                t  = t
toFahrenheit (Kelvin     t) = Fahrenheit $ (9 / 5 * t) - 459.67
toFahrenheit (Celsius    t) = Fahrenheit $ (9 / 5 * t) + 32
toFahrenheit             t  = t
