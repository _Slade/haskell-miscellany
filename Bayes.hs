{-# LANGUAGE UnicodeSyntax #-}
module Bayes
  where
import qualified Data.Map.Strict as M
import Data.Map.Strict (Map)
import Prelude hiding (not)

data Event =
      None
    | Not Event
    | B
    | A
    | Event `Or`    Event
    | Event `And`   Event
    | Event `Given` Event
    deriving (Eq, Ord, Show, Read)

infixl 2 `cup`; infixl 3 `cap`
cup, cap :: Event -> Event -> Event
(cup, cap) = fmap gen (Or, And)
    where
        gen f a b
          | a == b = a
          | otherwise = f (max a b) (min a b)

tri1st (a, _, _) = a
tri2nd (_, b, _) = b
tri3rd (_, _, c) = c

unBinOp :: Event -> (Event, Event -> Event -> Event, Event)
unBinOp (a `And`   b) = (a, And,   b)
unBinOp (a `Or`    b) = (a, Or,    b)
unBinOp (a `Given` b) = (a, Given, b)
unBinOp _ = error "Not a binop event"

not :: Event -> Event
not args = case args of
    a `And` b -> not a `cup` not b -- DeMorgan's
    a `Or`  b -> not a `cap` not b -- DeMorgan's
    Not a -> a                 -- Involution
    a     -> Not a

known :: Map Event (Maybe Double)
known = M.empty

isKnown :: Event -> Bool
isKnown e = M.lookup known e == Nothing

events :: [Event]
events =
    [
        A,
        not A,
        B,
        not B,
            A `cap`       B,
            A `cap`   not B,
        not A `cap`       B,
        not A `cap`   not B,
            A `cup`       B,
            A `cup`   not B,
        not A `cup`       B,
        not A `cup`   not B,
            A `Given`     B,
            B `Given`     A,
        not A `Given`     B,
        not A `Given` not B,
        not B `Given`     A,
        not B `Given` not A
    ]
