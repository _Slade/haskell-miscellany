module XGCD where
import Text.Printf

main = printSteps 40902 24140

-- TODO Make an actual useful vector type
data Thrector = TV Integer Integer Integer deriving (Eq, Show)

tvF :: (Integer -> Integer) -> Thrector -> Thrector
tvF f (TV u1 u2 u3) = TV (f u1) (f u2) (f u3)

tvFTv :: (Integer -> Integer -> Integer) -> Thrector -> Thrector -> Thrector
tvFTv f (TV u1 u2 u3) (TV v1 v2 v3) = TV (f u1 v1) (f u2 v2) (f u3 v3)

infixl 6 .-
(.-) :: Thrector -> Thrector -> Thrector
(.-) = tvFTv (-)

infixl 7 .*
(.*) :: Thrector -> Integer -> Thrector
tv .* q = tvF (* q) tv

xgcd :: Integer -> Integer -> Thrector
xgcd u v =
    let us = TV 1 0 u
        vs = TV 0 1 v
    in xgcd' us vs
    where
        xgcd' us (TV _ _ 0) = us -- Base case
        xgcd' us@(TV _ _ u3) vs@(TV _ _ v3) =
            let q = u3 `div` v3
            in xgcd' vs (us .- (vs .* q))

xgcdSteps :: Integer -> Integer -> [(Integer, Thrector, Thrector)]
xgcdSteps u v =
    let us = TV 1 0 u
        vs = TV 0 1 v
    in xgcd' [] us vs
    where
        xgcd' ts us vs@(TV _ _ 0) = (0, us, vs) : ts -- Base case
        xgcd' ts us@(TV _ _ u3) vs@(TV _ _ v3) =
            let q = u3 `div` v3
                us' = us .- (vs .* q)
            in xgcd' ((q, us, vs) : ts) vs us'

xgcdPretty :: Integer -> Integer -> String
xgcdPretty u v = let TV u1 u2 u3 = xgcd u v in
    printf "gcd(%d, %d) = %d*%d %s %d*%d = %d"
        u v  u1 u  (if u2 < 0 then "-" else "+")  (abs u2) v  u3

printSteps :: Integer -> Integer -> IO ()
printSteps u v = let ts = xgcdSteps u v 
    in putStrLn "q\tu1\tu2\tu3\tv1\tv2\tv3" >>
    mapM_ f (reverse ts)
    where
        f (q, (TV u1 u2 u3), (TV v1 v2 v3)) = putStrLn . untabs
            $ map show [q, u1, u2, u3, v1, v2, v3]

untabs []     =  ""
untabs (w:ws) = w ++ go ws
  where
    go []     = ""
    go (v:vs) = '\t' : (v ++ go vs)
