module Util where

flatZip :: [a] -> [a] -> [a]
flatZip []     _bs    = []
flatZip _as    []     = []
flatZip (a:as) (b:bs) = a : b : flatZip as bs

(&) :: a -> (a -> b) -> b
(&) = flip ($)
