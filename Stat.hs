module Stat (
    mean,
    mean2,
    -- median,
    quartile,
    recipm1,
    variance,
    stdDev,
    interQuartileRange,
    fiveNumSum,
    cartProd,
    binomial,
    factorial,
    perms,
    combs,
) where

import Data.List (
    sort,
    genericLength,
    intercalate,
    zipWith,
 )
import Data.Foldable (toList)
import Control.Exception.Base (throw, assert, ArithException(DivideByZero))
import Debug.Trace (traceShow)

emptyError :: a
emptyError = error "Sample must not be empty"

alwaysAssert cond f
    | cond = f
    | otherwise = error "Assertion failed"


genericQuartile
    :: (Integral a1, Fractional a)
    => (c a -> Bool)     -- Empty
    -> Int               -- Length
    -> (c a -> Int -> a) -- Indexing function
    -> c a               -- Sample
    -> a1                -- Quartile to pick
    -> a
genericQuartile empty n (!) xs q
    | empty xs  = emptyError
    | otherwise = genericMedian empty (!) xs start end
        where (start, end) = case q of {
            1 -> (0, quot n 2 - 1);
            2 -> (0, n - 1);
            3 -> (n - 1, uncurry (+) $ n `quotRem` 2);
            _ -> error "Unknown quartile (must be 1, 2, or 3)";
        }

genericMedian
    :: (Fractional a)
    => (c a -> Bool)     -- Empty
    -> (c a -> Int -> a) -- Indexing function
    -> c a               -- Sample
    -> Int               -- Start
    -> Int               -- End
    -> a
genericMedian empty get xs start end
    | empty xs    = emptyError
    | even length = getSafe xs mid `mean2` getSafe xs (mid - 1)
    | otherwise   = getSafe xs mid
        where
            length       = end - start
            mid          = length `div` 2
            getSafe xs n = alwaysAssert (n >= 0) (get xs n)

quartile :: (Fractional a) => Int -> [a] -> a
quartile q xs = genericQuartile null (length xs) (!!) xs q

{-
quartile :: (Fractional a, Ord a) => Int -> [a] -> a
quartile _ [] = emptyError
quartile n xs =
    case n of
        1 -> median (sort xs) 0 (len `div` 2 - 1)
        2 -> median (sort xs) 0 (len - 1)
        3 -> median (sort xs) (len - 1) (len `quot` 2 + len `rem` 2)
        _ -> error "Unknown quartile (must be 1, 2, or 3)"
    where len = length xs

median :: (Fractional a) => [a] -> Int -> Int -> a
median sample start end
    | null sample = emptyError
    | even len    = (geti mid + geti (mid - 1)) / 2
    | otherwise   = geti mid
    where
        len  = end - start + 1
        mid  = len `div` 2
        geti n = (sample !! (n + start))

quartiles :: (Fractional a, Ord a) => [a] -> [a]
quartiles xs = [ quartile n xs | n <- [1, 2, 3] ]
-}

mean2 :: (Fractional a) => a -> a -> a
mean2 x0 x1 = (x0 + x1) / 2.0

mean :: Fractional a => [a] -> a
mean [] = emptyError
mean xs = sum xs / genericLength xs

recipm1 :: (Eq a, Fractional a) => a -> a
recipm1 1 = throw DivideByZero -- Divide by zero
recipm1 n = recip $ n - 1

variance :: (Eq a, Fractional a) => [a] -> a
variance xs = prod * (sum $ map (\x -> (x - xMean) ^^ 2) xs)
    where
        xMean = mean xs
        prod  = recipm1 $ genericLength xs

-- XXX Find out what to do for a Fractional-compatible sqrt
stdDev :: (Eq a, Floating a) => [a] -> a
stdDev xs = sqrt $ variance xs

fiveNumSum :: [Double] -> FiveNumSum Double
fiveNumSum xs = Summarize
    (minimum xs) (quartile 1 xs) (quartile 2 xs) (quartile 3 xs) (maximum xs)

data FiveNumSum a = Summarize a a a a a
    deriving (Eq, Ord)

instance Functor FiveNumSum where
    fmap f (Summarize a b c d e) =
        Summarize (f a) (f b) (f c) (f d) (f e)

instance Foldable FiveNumSum where
    foldr (+) z (Summarize a b c d e) = (a + (b + (c + (d + (e + z)))))

instance Show a => Show (FiveNumSum a) where
    show fns@(Summarize min q1 q2 q3 max) =
        intercalate ", " . map join $ zip labels values
            where
                labels = [ "Mininum:", "Q1:", "Q2:", "Q3:", "Maximum:" ]
                values = fmap show $ toList fns
                join = uncurry ((++) . (++ " "))

interQuartileRange :: (Fractional a, Ord a) => [a] -> a
interQuartileRange xs = quartile 3 xs - quartile 1 xs

-- TODO: Test this
correlation :: (Eq a, Floating a) => [a] -> [a] -> a
correlation xs ys
    | length xs == length ys = error "Samples must have same size"
    | otherwise = prod * (sum $ zipWith (*) (map innerx xs) (map innery ys))
    where
        prod     = recipm1 $ genericLength xs
        xMean    = mean xs
        yMean    = mean ys
        xStdDev  = stdDev xs
        yStdDev  = stdDev ys
        innerx x = (x - xMean) / xStdDev
        innery y = (y - yMean) / yStdDev

cartProd :: [a] -> [b] -> [(a, b)]
cartProd xs ys = [ (x, y) | x <- xs, y <- ys ]

-- Approximation based on MacLaurin series
erf :: Double -> Double
erf x = (2 * x) / sqrt(pi)
    - (2 * x ^^ 3) / (3 * sqrt(pi))
    + x ^^  5 / (               5 * sqrt(pi))
    - x ^^  7 / (              21 * sqrt(pi))
    + x ^^  9 / (             108 * sqrt(pi))
    - x ^^ 11 / (             660 * sqrt(pi))
    + x ^^ 13 / (            4680 * sqrt(pi))
    - x ^^ 15 / (           37800 * sqrt(pi))
    + x ^^ 17 / (          342720 * sqrt(pi))
    - x ^^ 19 / (         3447360 * sqrt(pi))
    + x ^^ 21 / (        38102400 * sqrt(pi))
    - x ^^ 23 / (       459043200 * sqrt(pi))
    + x ^^ 25 / (      5987520000 * sqrt(pi))
    - x ^^ 27 / (     84064780800 * sqrt(pi))
    + x ^^ 29 / (   1264085222400 * sqrt(pi))
    - x ^^ 31 / (  20268952704000 * sqrt(pi))
    + x ^^ 33 / ( 345226033152000 * sqrt(pi))
    - x ^^ 35 / (6224529991680000 * sqrt(pi))

factorial :: (Num a, Enum a) => a -> a
factorial n = product [2 .. n]

binomial :: Integral a => a -> a -> a
binomial n k = factorial n `div` (factorial k * factorial (n - k))

perms = factorial

combs = binomial
